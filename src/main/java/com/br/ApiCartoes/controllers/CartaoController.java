package com.br.ApiCartoes.controllers;

import com.br.ApiCartoes.models.Cartao;
import com.br.ApiCartoes.models.Cliente;
import com.br.ApiCartoes.models.dtos.BuscarCartaoDTO;
import com.br.ApiCartoes.models.dtos.CartaoAtivoDTO;
import com.br.ApiCartoes.models.dtos.CartaoDTO;
import com.br.ApiCartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class CartaoController {
    @Autowired
    private CartaoService cartaoService;

    @PostMapping("/cartao")
    public ResponseEntity<Cartao> salvarCartao(@RequestBody @Valid CartaoDTO cartaoDTO){
        Cartao cartaoRetorno = cartaoService.salvarCartao(cartaoDTO);

        if(cartaoRetorno.getId() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }else {
            return ResponseEntity.status(201).body(cartaoRetorno);
        }
    }

    @PatchMapping("/cartao/{id}")
    public Cartao atualizarCartao(@PathVariable Integer id, @RequestBody CartaoAtivoDTO cartaoAtivoDTO){
        cartaoAtivoDTO.setId(id);
        Cartao cartaoRetorno = cartaoService.atualizarCartao(cartaoAtivoDTO);

        if(cartaoRetorno.getId() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }else{
            return cartaoRetorno;
        }
    }

    @GetMapping("/cartao/{id}")
    public BuscarCartaoDTO buscarCartao(@PathVariable Integer id){
        BuscarCartaoDTO buscarCartaoDTO = cartaoService.buscarCartao(id);
        if(buscarCartaoDTO.getId() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        else{
            return buscarCartaoDTO;
        }
    }
}
