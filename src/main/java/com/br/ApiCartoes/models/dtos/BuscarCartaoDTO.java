package com.br.ApiCartoes.models.dtos;

public class BuscarCartaoDTO {

    private Integer id;

    private Integer numero;

    private Integer clienteId;

    public BuscarCartaoDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
