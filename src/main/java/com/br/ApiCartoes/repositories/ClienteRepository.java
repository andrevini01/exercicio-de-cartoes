package com.br.ApiCartoes.repositories;

import com.br.ApiCartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
