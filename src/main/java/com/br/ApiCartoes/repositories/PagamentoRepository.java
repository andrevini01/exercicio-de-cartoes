package com.br.ApiCartoes.repositories;

import com.br.ApiCartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findByCartaoId(Integer cartaoId);
}
