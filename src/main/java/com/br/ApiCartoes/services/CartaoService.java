package com.br.ApiCartoes.services;

import ch.qos.logback.core.pattern.parser.OptionTokenizer;
import com.br.ApiCartoes.models.Cartao;
import com.br.ApiCartoes.models.Cliente;
import com.br.ApiCartoes.models.dtos.BuscarCartaoDTO;
import com.br.ApiCartoes.models.dtos.CartaoAtivoDTO;
import com.br.ApiCartoes.models.dtos.CartaoDTO;
import com.br.ApiCartoes.repositories.CartaoRepository;
import com.br.ApiCartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public Cartao salvarCartao(CartaoDTO cartaoDTO){
        Optional<Cliente> clienteOptional = clienteService.buscarCliente(cartaoDTO.getClienteId());
        Cartao cartao = new Cartao();
        Cartao cartaoRetorno = new Cartao();

        if(clienteOptional.isPresent()){
            cartao.setNumero(cartaoDTO.getNumero());
            cartao.setClienteId(clienteOptional.get().getId());
            cartao.setAtivo(false);
            cartaoRetorno = cartaoRepository.save(cartao);
        }
        return cartaoRetorno;
    }

    public Cartao atualizarCartao(CartaoAtivoDTO cartaoAtivoDTO){
        BuscarCartaoDTO buscarCartaoDTO = buscarCartao(cartaoAtivoDTO.getId());
        Cartao cartaoData = new Cartao();

        if(buscarCartaoDTO.getId() != null){
            cartaoData.setId(buscarCartaoDTO.getId());
            cartaoData.setNumero(buscarCartaoDTO.getNumero());
            cartaoData.setClienteId(buscarCartaoDTO.getClienteId());
            cartaoData.setAtivo(cartaoAtivoDTO.isAtivo());
        }else{
            return new Cartao();
        }

        Cartao cartaoRetorno = cartaoRepository.save(cartaoData);
        return cartaoRetorno;
    }

    public BuscarCartaoDTO buscarCartao(Integer id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        BuscarCartaoDTO buscarCartaoDTO = new BuscarCartaoDTO();

        if(cartaoOptional.isPresent()){
            buscarCartaoDTO.setId(cartaoOptional.get().getId());
            buscarCartaoDTO.setClienteId(cartaoOptional.get().getClienteId());
            buscarCartaoDTO.setNumero(cartaoOptional.get().getNumero());
        }
        return buscarCartaoDTO;
    }
}
