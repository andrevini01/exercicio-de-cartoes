package com.br.ApiCartoes.repositories;

import com.br.ApiCartoes.models.Cartao;
import com.br.ApiCartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
}
