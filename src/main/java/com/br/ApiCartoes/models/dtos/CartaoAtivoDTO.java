package com.br.ApiCartoes.models.dtos;

public class CartaoAtivoDTO {
    private Integer id;
    private boolean ativo;

    public CartaoAtivoDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
