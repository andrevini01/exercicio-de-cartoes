package com.br.ApiCartoes.controllers;

import com.br.ApiCartoes.models.Cliente;
import com.br.ApiCartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping("/cliente")
    public ResponseEntity<Cliente> salvarCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteRetorno = clienteService.salvarCliente(cliente);
        return ResponseEntity.status(201).body(clienteRetorno);
    }

    @GetMapping("/cliente/{id}")
    public Cliente buscarCliente(@PathVariable Integer id){
        Optional<Cliente> clienteOptional = clienteService.buscarCliente(id);
        if(clienteOptional.isPresent()){
            return clienteOptional.get();
        }
        else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
