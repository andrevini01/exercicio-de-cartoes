package com.br.ApiCartoes.controllers;

import com.br.ApiCartoes.models.Cartao;
import com.br.ApiCartoes.models.Cliente;
import com.br.ApiCartoes.models.Pagamento;
import com.br.ApiCartoes.models.dtos.CartaoDTO;
import com.br.ApiCartoes.services.ClienteService;
import com.br.ApiCartoes.services.PagamentoService;
import com.sun.corba.se.spi.legacy.connection.GetEndPointInfoAgainException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public ResponseEntity<Pagamento> salvarPagamento(@RequestBody @Valid Pagamento pagamento){
        Pagamento pagamentoRetorno = pagamentoService.salvarPagamento(pagamento);

        if(pagamentoRetorno.getId() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }else {
            return ResponseEntity.status(201).body(pagamentoRetorno);
        }
    }

    @GetMapping("/pagamentos/{id}")
    public Iterable<Pagamento> buscarCliente(@PathVariable Integer id){
        Iterable<Pagamento> pagamentoIterable = pagamentoService.buscarPagamentos(id);
        if(pagamentoIterable != null){
            return pagamentoIterable;
        }
        else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
