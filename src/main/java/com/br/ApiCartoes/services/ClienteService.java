package com.br.ApiCartoes.services;


import com.br.ApiCartoes.models.Cliente;
import com.br.ApiCartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        Cliente clienteRetorno = clienteRepository.save(cliente);
        return clienteRetorno;
    }

    public Optional<Cliente> buscarCliente(Integer id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        return clienteOptional;
    }

}
