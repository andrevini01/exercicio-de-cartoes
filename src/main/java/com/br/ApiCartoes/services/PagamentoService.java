package com.br.ApiCartoes.services;

import com.br.ApiCartoes.models.Pagamento;
import com.br.ApiCartoes.models.dtos.BuscarCartaoDTO;
import com.br.ApiCartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public Pagamento salvarPagamento(Pagamento pagamento){
        BuscarCartaoDTO buscarCartaoDTO = cartaoService.buscarCartao(pagamento.getCartaoId());
        Pagamento pagamentoRetorno = new Pagamento();

        if(buscarCartaoDTO.getId() != null){
            pagamentoRetorno = pagamentoRepository.save(pagamento);
        }
        return pagamentoRetorno;
    }

    public Iterable<Pagamento> buscarPagamentos(Integer idCartao){
        Iterable<Pagamento> pagamentoIterable = pagamentoRepository.findByCartaoId(idCartao);
        return pagamentoIterable;
    }
}
